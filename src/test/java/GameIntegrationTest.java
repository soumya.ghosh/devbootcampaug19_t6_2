import org.junit.Test;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;


public class GameIntegrationTest {
    Game game;

    @Test
    public void shouldTestFirstRoundWithCoopAndCpyCat(){
        Player  player1 = new Player(new ConsoleBehaviour(new Scanner("COOPERATE")));
        Player player2 = new Player(new CopyCatBehaviourImpl());
        Machine machine = new Machine();
        int noOfRounds = 1;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:2 Score of second player: 2");

    }
    /*@Test
    public void shouldTestFirstRoundWithCheatAndCpyCat(){
        Player  player1 = new Player(new ConsoleBehaviour(new Scanner("CHEAT")));
        CopyCatBehaviourImpl behaviour = new CopyCatBehaviourImpl();
        Player player2 = new Player(behaviour);
        Machine machine = new Machine();
        int noOfRounds = 1;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(behaviour);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:0 Score of second player: 0");

    }*/
    @Test
    public void shouldTestFirstRoundWithCopyCatAsFirstPlayer(){
        Player  player2 = new Player(new ConsoleBehaviour(new Scanner("CHEAT")));
        Player player1 = new Player(new CopyCatBehaviourImpl());
        Machine machine = new Machine();
        int noOfRounds = 1;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:-1 Score of second player: 3");

    }

    @Test
    public void shouldTestFiveRoundWithCopyCatAsFirstPlayer(){
        Player  player2 = new Player(()-> Move.CHEAT.toString());
        CopyCatBehaviourImpl behaviour = new CopyCatBehaviourImpl();
        Player player1 = new Player(behaviour);
        Machine machine = new Machine();
        int noOfRounds = 5;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(behaviour);
        //test play
        game.play();

        //verify the console output
        verify(console,times(5)).println("Score of first player:-1 Score of second player: 3");


    }

    @Test
    public void shouldTestFiveRoundWithCopyCatAsFirstPlayerAndSecondAlwaysCooperating(){
        Player  player2 = new Player(()-> Move.COOPERATE.toString());
        Player player1 = new Player(new CopyCatBehaviourImpl());
        Machine machine = new Machine();
        int noOfRounds = 5;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:2 Score of second player: 2");
        verify(console).println("Score of first player:4 Score of second player: 4");
        verify(console).println("Score of first player:6 Score of second player: 6");
        verify(console).println("Score of first player:8 Score of second player: 8");
        verify(console).println("Score of first player:10 Score of second player: 10");


    }
}