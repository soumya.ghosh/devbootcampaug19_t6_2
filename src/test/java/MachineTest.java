import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MachineTest {
    Machine machine;

    @Before
    public void setup() {
        machine = new Machine();
    }

    @Test
    public void shouldTestCalculateScoreForBothCooperate(){
        List<Integer> result = new ArrayList<Integer>();
        result.add(2);
        result.add(2);
        List<Integer> score = machine.calculateScore(Move.COOPERATE.toString(),Move.COOPERATE.toString());
        Assert.assertEquals(result, score);
    }

    @Test
    public void shouldGetCalculatedScoreForBothCheating() {
        List<Integer> result2 = new ArrayList<Integer>();
        result2.add(0);
        result2.add(0);
        Assert.assertEquals(result2, machine.calculateScore(Move.CHEAT.toString(),Move.CHEAT.toString()));
    }

    @Test
    public void shouldGetCalculatedScoreForPlayer1CooperatePlayer2Cheat() {
        List<Integer> result2 = new ArrayList<Integer>();
        result2.add(-1);
        result2.add(3);
        Assert.assertEquals(result2, machine.calculateScore(Move.COOPERATE.toString(),Move.CHEAT.toString()));
    }

    @Test
    public void shouldGetCalculatedScoreForPlayer2CooperatePlayer1Cheat() {
        List<Integer> result2 = new ArrayList<Integer>();
        result2.add(3);
        result2.add(-1);
        Assert.assertEquals(result2, machine.calculateScore(Move.CHEAT.toString(),Move.COOPERATE.toString()));
    }
}
