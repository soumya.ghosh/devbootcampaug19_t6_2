import org.junit.Test;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class GameTest {
    Game game;

    @Test
    public void shouldReturnScore(){
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        int noOfRounds = 1;
        PrintStream console = mock(PrintStream.class);
        List<Integer> result2 = new ArrayList<Integer>();
        result2.add(3);
        result2.add(-1);
        when(player1.move()).thenReturn("CHEAT");
        when(player2.move()).thenReturn("COOPERATE");

        when(machine.calculateScore("CHEAT", "COOPERATE")).thenReturn(result2);

        game = new Game(player1, player2, machine, noOfRounds, console);
        //test play
        game.play();
        verify(player1).move();
        verify(player2).move();
        verify(machine).calculateScore("CHEAT", "COOPERATE");
    }
}
