import org.junit.Assert;
import org.junit.Test;

public class CopyCatBehaviourTest {

    @Test
    public void findOpponentsMove() {
        CopyCatBehaviourImpl c = new CopyCatBehaviourImpl();
        c.setMyMove(Move.CHEAT);
        Move othersMove = c.findOpponentsMove(new Move[] {Move.CHEAT, Move.COOPERATE});
        Assert.assertEquals(Move.COOPERATE, othersMove);
    }
}
