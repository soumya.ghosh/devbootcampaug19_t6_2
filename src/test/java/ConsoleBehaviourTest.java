import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class ConsoleBehaviourTest {
    ConsoleBehaviour player;

    @Test
    public void shouldTestPlayerCooperate(){
        player = new ConsoleBehaviour(new Scanner("COOPERATE"));
        Assert.assertEquals("COOPERATE", player.move());

    }

    @Test
    public void shouldTestPlayerCheat(){
        player = new ConsoleBehaviour(new Scanner("CHEAT"));
        Assert.assertEquals("CHEAT", player.move());
    }
}
