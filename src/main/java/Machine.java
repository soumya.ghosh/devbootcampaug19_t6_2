import java.util.ArrayList;
import java.util.List;

public class Machine {

    public List<Integer> calculateScore(String moveOfPlayer1, String moveOfPlayer2) {
        //Define Rules

        // Calculate acc to rules

        //give score in arrya format
        List<Integer> score = new ArrayList<Integer>();
        if(moveOfPlayer1.equals(Move.COOPERATE.toString()) && moveOfPlayer2.equals(Move.COOPERATE.toString())){
            score.add(2);
            score.add(2);
        } else if(moveOfPlayer1.equals(Move.CHEAT.toString()) && moveOfPlayer2.equals(Move.CHEAT.toString())){
            score.add(0);
            score.add(0);
        } else if(moveOfPlayer1.equals(Move.CHEAT.toString()) && moveOfPlayer2.equals(Move.COOPERATE.toString())){
            score.add(3);
            score.add(-1);
        }  else {
            score.add(-1);
            score.add(3);
        }
        return score;
    }
}
