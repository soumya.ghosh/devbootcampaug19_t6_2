import java.util.Scanner;

// this is observable
public class Player implements StateObservable {

    private int score;
    private Behaviour behaviour;
    private String lastState;

    Player( Behaviour behaviour){
        this.behaviour = behaviour;

        this.score = 0;
    }

    public String move() {
        lastState = behaviour.move();
        return lastState ;
    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score += score;
    }

    public String getLastState() {
        return lastState;
    }
}