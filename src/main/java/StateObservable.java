public interface StateObservable {
    public String getLastState();
}
