import org.graalvm.compiler.lir.aarch64.AArch64Move;

import java.util.Observable;
import java.util.Observer;

// This is now Observer
public class CopyCatBehaviourImpl implements Behaviour, Observer {
    private Move myMove = Move.COOPERATE;

    @Override
    public String move() {
        return myMove.toString();
    }

    public void setMyMove(Move move) {
        this.myMove = move;
    }

    public Move findOpponentsMove(Move[] moves) {
        return moves[0] == myMove ? moves[1] : moves[0];
    }

    @Override
    public void update(Observable o, Object arg) {
        Move[] moves = (Move[]) arg;
        myMove = findOpponentsMove(moves);
    }
}
