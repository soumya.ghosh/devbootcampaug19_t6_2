import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Game extends Observable {


    private final Player player1;
    private final Player player2;

    private final Machine machine;
    private int noOfRounds;
    private final PrintStream console;


    public Game(Player player1, Player player2, Machine machine, int noOfRounds, PrintStream console) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfRounds = noOfRounds;
        this.console = console;
    }


    public void playRound() {


        String player1Move = player1.move();
        String player2Move = player2.move();

        //give score to engine to calculate
        List<Integer> score  = machine.calculateScore(player1Move, player2Move);
        //print the score
        player1.setScore(score.get(0));
        player2.setScore(score.get(1));
        console.println("Score of first player:"+player1.getScore()+" Score of second player: "+player2.getScore());
        //console.println("-----------------------------------------------------------------");
        setChanged();
        notifyObservers(new Move[] { Move.valueOf(player1Move), Move.valueOf(player2Move)});
    }

    public void play(){
        while (noOfRounds>0) {
            playRound();
            noOfRounds--;
        }
    }

}
