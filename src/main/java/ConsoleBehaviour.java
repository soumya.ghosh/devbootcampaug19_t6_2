import java.util.Scanner;

public class ConsoleBehaviour implements Behaviour{

    Scanner scanner;


    public ConsoleBehaviour(Scanner scanner) {
        this.scanner = scanner;
    }

    public String move() {
        return scanner.nextLine();
    }
}